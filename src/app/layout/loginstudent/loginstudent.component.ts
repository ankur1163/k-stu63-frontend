import { Component, OnInit,Inject } from '@angular/core';
import {loginStudent} from '../loginstudent';
import {HttpClient}  from '@angular/common/http';
import { SESSION_STORAGE,StorageService} from 'ngx-webstorage-service';
import {Router} from '@angular/router';

import { Apollo } from 'apollo-angular';
import {CookieService} from 'ngx-cookie-service';
let inMemoryToken;

@Component({
  selector: 'app-loginstudent',
  templateUrl: './loginstudent.component.html',
  styleUrls: ['./loginstudent.component.scss']
})
export class LoginstudentComponent implements OnInit {
  private tokenValue :string;
  constructor(private CookieService:CookieService,private apollo: Apollo,private router:Router,private http:HttpClient,@Inject(SESSION_STORAGE) private storage: StorageService) { }
  loginStudent = new loginStudent("","")
  
  submitLoginForm(){
    console.log("login form submitted",this.loginStudent.Email,this.loginStudent.password)
    let params = {
      "email":this.loginStudent.Email,
      "password":this.loginStudent.password
    }

    this.http.post<any>("http://localhost:3000/api/user/login",params).pipe().subscribe(resp=>{
      console.log("resp.status",resp, resp.status)
    if(resp.message){
      inMemoryToken ={
        token:resp.message
      }
      localStorage.setItem('token', resp.message);      this.router.navigate(["/homestudent"])

    }  
    else{
      alert("your username or password is wrong, try again")

    }
    
    })
  }
  ngOnInit(){

  }


}
