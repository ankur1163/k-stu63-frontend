import { NgModule } from '@angular/core';

import { VerticalLayout1Module } from 'app/layout/vertical/layout-1/layout-1.module';
import { VerticalLayout2Module } from 'app/layout/vertical/layout-2/layout-2.module';
import { VerticalLayout3Module } from 'app/layout/vertical/layout-3/layout-3.module';

import { HorizontalLayout1Module } from 'app/layout/horizontal/layout-1/layout-1.module';
import { SignupPageComponent } from './signup-page/signup-page.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {NgxWebstorageModule} from 'ngx-webstorage';
import { LoginstudentComponent } from './loginstudent/loginstudent.component';
import { ApolloModule, APOLLO_OPTIONS } from "apollo-angular";
 import { HttpLinkModule, HttpLink } from "apollo-angular-link-http";
 import { InMemoryCache } from "apollo-cache-inmemory";
 import { ApolloClient } from 'apollo-client';

console.log("ff",localStorage.getItem("token"))
var tk = localStorage.getItem("token");

@NgModule({
    imports: [
        VerticalLayout1Module,
        VerticalLayout2Module,
        VerticalLayout3Module,
        NgxWebstorageModule.forRoot(),
        FlexLayoutModule,
       FormsModule,
       HttpClientModule,
       
        ApolloModule,
        HttpLinkModule,

        HorizontalLayout1Module
    ],
    providers: [{
        provide: APOLLO_OPTIONS,
        useFactory: (httpLink) => {
          return new ApolloClient({
            cache: new InMemoryCache(),
            link:  httpLink.create({
              uri: 'https://edu-2020.herokuapp.com/v1/graphql',
              withCredentials:true,
                headers: {
                    //"x-hasura-admin-secret":"Salhotra1"
                 Authorization: `Bearer ${tk}`
                  //Authorization: `Bearer ${console.log(CookieService.)}`
                }
            })
          })
        },
        deps: [HttpLink]
      }],
    exports: [
        VerticalLayout1Module,
        VerticalLayout2Module,
        VerticalLayout3Module,

        HorizontalLayout1Module
    ],
    declarations: [SignupPageComponent, LoginstudentComponent]
})
export class LayoutModule
{
}
