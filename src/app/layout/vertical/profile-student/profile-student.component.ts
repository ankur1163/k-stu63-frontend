import { Component, OnInit } from '@angular/core';
import {profileStudent} from '../../profileStudent';
import {HttpClient} from '@angular/common/http';
import gql from 'graphql-tag';

const GET_MY_TODOS = gql`
 query MyQuery {
    user_user {
      
      id
      role
    }
  }`;

@Component({
  selector: 'app-profile-student',
  templateUrl: './profile-student.component.html',
  styleUrls: ['./profile-student.component.scss']
})
export class ProfileStudentComponent implements OnInit {

  constructor(private http: HttpClient) { }
  profileStudentInfo = new profileStudent("",null,"","");
 
  saveProfile(){
    console.log("profile submitted")
    let params = {
      "fullname":this.profileStudentInfo.fullname,
      "address":this.profileStudentInfo.address,
      "email":this.profileStudentInfo.email,
      "Phonenumber":this.profileStudentInfo.phonenumber


    }
  }
  ngOnInit(): void {
  }

}
