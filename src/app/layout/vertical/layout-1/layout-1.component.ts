import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { FuseConfigService } from '@fuse/services/config.service';
import { navigation } from 'app/navigation/navigation';
import gql from 'graphql-tag';
import { Apollo } from 'apollo-angular';

import {getCookie} from '../../helper/getCookie';



 const GET_MY_TODOS = gql`
 query MyQuery {
    user_user(where: {id: {_eq: ""}}) {
      email
      id
      role
      fullname
      address
      Phonenumber
    }
  }`;


@Component({
    selector     : 'vertical-layout-1',
    templateUrl  : './layout-1.component.html',
    styleUrls    : ['./layout-1.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class VerticalLayout1Component implements OnInit, OnDestroy
{
    fuseConfig: any;
    navigation: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * cookievalue;
     */
    cookieValue;
    allcookies;
    
    constructor(
        
        
        private apollo: Apollo,
        private _fuseConfigService: FuseConfigService
    )
    {
        const gt = new getCookie();
        console.log("getCookie",gt.gettoken());
        
       
      
        console.log("yes")
        
        // Set the defaults
        this.navigation = navigation;

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
       
            
        
       
        // Subscribe to config changes
        this._fuseConfigService.config
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((config) => {
                this.fuseConfig = config;
            });
            this.apollo.watchQuery<any>({
                query: GET_MY_TODOS
              })
               .valueChanges
               .subscribe(({ data, loading }) => {
                  console.log("data f",data)
                   
              });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}
