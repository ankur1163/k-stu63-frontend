import { Component, OnInit,Inject } from '@angular/core';
import {signupStudent} from '../signupstudent';
import {HttpClient} from '@angular/common/http';
import { SESSION_STORAGE, StorageService } from 'ngx-webstorage-service';
import { Router } from '@angular/router';
import {CookieService} from 'ngx-cookie-service';

let inMemoryToken;

@Component({
  selector: 'app-signup-page',
  templateUrl: './signup-page.component.html',
  styleUrls: ['./signup-page.component.scss']
})
export class SignupPageComponent implements OnInit {

  constructor(private CookieService:CookieService,private router:Router,private http: HttpClient, @Inject(SESSION_STORAGE) private storage: StorageService) { }
  studentSignupInfo = new signupStudent("","","","");
  
  
  submitSignupForm(){
    console.log("submitted")
    let params= {
      "name":this.studentSignupInfo.Fullname,
      "email":this.studentSignupInfo.email,
       "password":this.studentSignupInfo.password
    }
    this.http.post<any>("http://localhost:3000/api/user/register",params).pipe().subscribe(resp =>{ 
    //document.cookie = "token"  + "=" + data.message 
    console.log("resp.status",resp, resp.status)
    if(resp.message){
      inMemoryToken ={
        token:resp.message
      }
      localStorage.setItem("token",resp.message)
      this.router.navigate(["/homestudent"])

    } 
    else{
      alert("Something is wrong")

    }
  })
}

  ngOnInit(): void {
    
  }

}
