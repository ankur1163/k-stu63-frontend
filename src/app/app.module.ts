import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';
import { InMemoryCache } from "apollo-cache-inmemory";

import 'hammerjs';

import { FuseModule } from '@fuse/fuse.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule } from '@fuse/components';

import { fuseConfig } from 'app/fuse-config';
import {VerticalLayout1Component} from './layout/vertical/layout-1/layout-1.component'
import { LoginstudentComponent} from './layout/loginstudent/loginstudent.component';
import { AppComponent } from 'app/app.component';
import { LayoutModule } from 'app/layout/layout.module';
import { SampleModule } from 'app/main/sample/sample.module';
import { ApolloModule, APOLLO_OPTIONS } from "apollo-angular";
 import { HttpLinkModule, HttpLink } from "apollo-angular-link-http";
import {SignupPageComponent} from "./layout/signup-page/signup-page.component"
 import { ApolloClient } from 'apollo-client';
 import { FlexLayoutModule } from '@angular/flex-layout';
 import {ProfileStudentComponent} from './layout/vertical/profile-student/profile-student.component'

const appRoutes: Routes = [
    { path: 'signup', component: SignupPageComponent },
    { path: 'homestudent', component: VerticalLayout1Component,
        children:[{
            path:'profilestudent',
            component:ProfileStudentComponent
        } ]
    },
    { path: 'loginstudent',component:LoginstudentComponent},
    {
        path      : 'home',
        redirectTo: 'sample'
    }
];

@NgModule({
    declarations: [
        AppComponent
    ],
    imports     : [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterModule.forRoot(appRoutes),

        TranslateModule.forRoot(),
        HttpClientModule,
        ApolloModule,
        HttpLinkModule,
        FlexLayoutModule,

        // Material moment date module
        MatMomentDateModule,

        // Material
        MatButtonModule,
        MatIconModule,

        // Fuse modules
        FuseModule.forRoot(fuseConfig),
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,

        // App modules
        LayoutModule,
        SampleModule
    ],
 
    bootstrap   : [
        AppComponent
    ]
})
export class AppModule
{
}
